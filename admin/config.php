<?php
// HTTP
define('HTTP_SERVER', 'http://chic.od.ua/admin/');
define('HTTP_CATALOG', 'http://chic.od.ua/');

// HTTPS
define('HTTPS_SERVER', 'http://chic.od.ua/admin/');
define('HTTPS_CATALOG', 'http://chic.od.ua/');

// DIR
define('DIR_APPLICATION', 'E:/yura/OSPanel/domains/chic.od.ua/admin/');
define('DIR_SYSTEM', 'E:/yura/OSPanel/domains/chic.od.ua/system/');
define('DIR_IMAGE', 'E:/yura/OSPanel/domains/chic.od.ua/image/');
define('DIR_LANGUAGE', 'E:/yura/OSPanel/domains/chic.od.ua/admin/language/');
define('DIR_TEMPLATE', 'E:/yura/OSPanel/domains/chic.od.ua/admin/view/template/');
define('DIR_CONFIG', 'E:/yura/OSPanel/domains/chic.od.ua/system/config/');
define('DIR_CACHE', 'E:/yura/OSPanel/domains/chic.od.ua/system/storage/cache/');
define('DIR_DOWNLOAD', 'E:/yura/OSPanel/domains/chic.od.ua/system/storage/download/');
define('DIR_LOGS', 'E:/yura/OSPanel/domains/chic.od.ua/system/storage/logs/');
define('DIR_MODIFICATION', 'E:/yura/OSPanel/domains/chic.od.ua/system/storage/modification/');
define('DIR_UPLOAD', 'E:/yura/OSPanel/domains/chic.od.ua/system/storage/upload/');
define('DIR_CATALOG', 'E:/yura/OSPanel/domains/chic.od.ua/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'admin');
define('DB_PASSWORD', '123456');
define('DB_DATABASE', 'chic_bd');
define('DB_PORT', '3307');
define('DB_PREFIX', 'oc_');
