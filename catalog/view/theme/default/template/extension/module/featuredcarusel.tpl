<div class="container product-carusel">
  <?php if ($heading_title) { ?>
<h3><?php echo $heading_title; ?></h3>
  <?php } ?>
<div id="prodcarousel<?php echo $module; ?>" class="owl-carousel productcarusel view<?php echo $prodview; ?> <?php echo $class; ?>">
  <?php foreach ($products as $product) { ?>
      <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
  <?php } ?>
</div>
</div>
<script type="text/javascript"><!--
$('#prodcarousel<?php echo $module; ?>').owlCarousel({
  items: <?php echo $items; ?>,
  autoPlay: 3000,
  navigation: true,
  navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
  pagination: false
});
--></script>
