<footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-4">
        <img src="http://chic.od.ua/catalog/view/theme/default/image/icon/footlogo.png">
        <ul class="left-foot list-unstyled">
          <li><img src="http://chic.od.ua/catalog/view/theme/default/image/icon/home.png">АДРЕСС: ОДЕССА, УЛ. БАЗОВАЯ, 17В</li>
          <li><img src="http://chic.od.ua/catalog/view/theme/default/image/icon/plane.png">ПИШИТЕ: INFO@CHIC.NET.UA</li>
          <li><img src="http://chic.od.ua/catalog/view/theme/default/image/icon/phone.png">ЗВОНИТЕ: +38 (093) 493 67 91</li>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-2">
        <h5>КАБИНЕТ ПОЛЬЗОВАТЕЛЯ</h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-2">
        <h5>ИНФОРМАЦИЯ</h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <div class="col-sm-4">
        <h5>НОВОСТИ И РАССЫЛКИ</h5>
        <p>Получайте первыми информацию о наших новых продуктах, специальных предложениях и акциях</p>
        <label class="email" for="email">Введите Ваш E-mail
          <button  class="pull-right"><img src="http://chic.od.ua/catalog/view/theme/default/image/icon/plane.png"></button>
        </label>
      </div>
    </div>
  </div>
  <div class="foot2">
  <div class="row">
    <div class="social-f col-sm-4"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
    <div class="social-f col-sm-4"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
    <div class="social-f col-sm-4"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
  </div>
  </div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->



  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body></html>